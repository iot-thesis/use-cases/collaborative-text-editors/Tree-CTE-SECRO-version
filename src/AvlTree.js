require("google-closure-library");
goog.require("goog.structs.AvlTree");
goog.require("goog.structs.AvlTree.Node");

goog.structs.AvlTree.prototype.copy = function() {
    const tree = new goog.structs.AvlTree();

    // Copy instance properties
    const {copy, leftMost, rightMost} = this.root_.copy();
    tree.root_       = copy;
    tree.comparator_ = this.comparator_;
    tree.minNode_    = leftMost;
    tree.maxNode_    = rightMost;

    return tree;
};

goog.structs.AvlTree.Node.prototype.copy = function(parent) {
    const val  = JSON.parse(JSON.stringify(this.value)); // deep copy
    const node = new goog.structs.AvlTree.Node(val, parent);

    // Copy all properties
    node.count  = this.count;
    node.height = this.height;

    var minNode = node, maxNode = node;

    if (this.left) {
        const {copy, leftMost, rightMost} = this.left.copy(node);
        node.left = copy;
        minNode = leftMost;
    }

    if (this.right) {
        const {copy, leftMost, rightMost} = this.right.copy(node);
        node.right = copy;
        maxNode = rightMost;
    }

    return {copy: node, leftMost: minNode, rightMost: maxNode};
};

/*
 * Represents the binary tree in an array.
 * The tree is made complete by replacing
 * missing nodes (holes) by a dummy node.
 * The implementation corresponds to:
 * http://www.cs.northwestern.edu/academics/courses/311/html/tree-notes.html
 *
 * Also serializes the comparator function.
 * Therefore, the comparator should not rely on its surrounding scope!
 */
goog.structs.AvlTree.prototype.toJSON = function() {
    const array = [];
    this.breadthFirstTraverse(node => array.push(node));
    return array;
};

goog.structs.AvlTree.fromJSON = function(array, comparator) {
    function makeNode(arr, idx, parent) {
        const encodedNode = arr[idx];

        if (encodedNode === null) {
            // dummy node
            return {node: null};
        }
        else {
            const node = new goog.structs.AvlTree.Node(encodedNode, parent);
            var leftMost = node, rightMost = node;

            if ((2*idx + 2) < array.length) {
                const l = makeNode(arr, 2*idx + 1, node);
                const r = makeNode(arr, 2*idx + 2, node);
                node.left  = l.node;
                node.right = r.node;

                var lHeight = l.node ? l.node.height : 0;
                var rHeight = r.node ? r.node.height : 0;

                var lCount = l.node ? l.node.count : 0;
                var rCount = r.node ? r.node.count : 0;

                if (node.left !== null) {
                    leftMost = l.minNode;
                    lHeight = node.left.height;
                }

                if (node.right !== null) {
                    rightMost = r.maxNode;
                    rHeight = node.right.height;
                }

                node.height = Math.max(lHeight, rHeight) + 1;
                node.count  = 1 + lCount + rCount;
            }
            else {
                // We're a leaf node
                node.height = 1;
                node.count  = 1;
            }

            // Return leftmost, right most and node
            return {node: node, minNode: leftMost, maxNode: rightMost};
        }
    }

    var tree = new goog.structs.AvlTree(comparator);
    if (array.length > 0) {
        const {node, minNode, maxNode} = makeNode(array, 0, null);

        tree.root_    = node;
        tree.minNode_ = minNode;
        tree.maxNode_ = maxNode;
    }

    return tree;
};

goog.structs.AvlTree.prototype.breadthFirstTraverse = function(fn) {
    var currentLevel = [this.root_];
    var nextLevel = [];

    for (var level = 1, h = this.getHeight(); level <= h; level++) {
        // Process nodes of the current level
        for (var i = 0, n = currentLevel.length; i < n; i++) {
            const node = currentLevel[i];
            fn(node.value);
            // Replace missing children by dummy nodes.
            // Because we want a complete tree (i.e. do not skip gaps!).
            const left = node.left ? node.left : {value: null};
            const right = node.right ? node.right : {value: null};
            nextLevel.push(left);
            nextLevel.push(right);
        }

        // Next level becomes the current level
        currentLevel = nextLevel;
        nextLevel = [];
    }
};

module.exports = goog.structs.AvlTree;