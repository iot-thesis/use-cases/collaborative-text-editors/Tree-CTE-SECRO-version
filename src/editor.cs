/* 
 * 1) Compile this file
 *   "$(npm bin)"/csc "$PWD" ./src/editor.cs compiled
 * 2) Run this file
 *   node ./compiled/editor.js
 * 3) Wait for the "Received text editor service" message
 * 4) Open 'textEditor.html' in the browser.
 */

const { init, pushUpdate } = require('./communication');

/*
 * Subscribe to text editor services.
 */

const {Character, TextEditor} = require('./logic');

var editorService = null;
init(null, Character);

deftype TextEditor

subscribe TextEditor with editor => {
    if (editorService === null) {
        init(editor, Character);
        console.log('Received text editor service. You can now start editing the document');
        editorService = editor;
        editor.textEditor.onUpdate(pushUpdate);
    }
    else {
        console.log('[WARNING]: Already found a text editor service. Ignoring this one.');
    }
};