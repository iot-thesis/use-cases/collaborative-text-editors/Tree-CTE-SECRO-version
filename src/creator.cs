/*
 * 1) Compile this file
 *   "$(npm bin)"/csc "$PWD" ./src/creator.cs compiled
 * 2) Run this file
 *   node ./compiled/creator.js
 * 3) Open 'textEditor.html' in the browser.
 */

const { init, pushUpdate } = require('./communication');

/*
 * Create a TextEditor service and publish it.
 */

const {TextEditor, Character} = require('./logic');
deftype TextEditor

service TextEditorService {
    rep textEditor = new TextEditor();
}

var editorService = new TextEditorService();
init(editorService, Character);

editorService.textEditor.onUpdate(pushUpdate);
publish editorService as TextEditor